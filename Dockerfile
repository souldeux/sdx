FROM node:current-alpine
RUN apk update
RUN apk upgrade
RUN apk --no-cache --update add \
  gzip
WORKDIR /opt/sdx
ADD . /opt/sdx
WORKDIR /opt/sdx
RUN npm install
