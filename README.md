# sdx

Vue-based static app designed for deployment into / service from an S3 bucket.

### Master

[![pipeline status](https://gitlab.com/souldeux/sdx/badges/master/pipeline.svg)](https://gitlab.com/souldeux/sdx/commits/master)

When this pipeline is green, Master matches http://dabbing.cricket

Works with the [sdx-backend API](https://gitlab.com/souldeux/sdx-backend), hosted at http://api.dabbing.cricket.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Deploy to S3 Bucket
Note: the `vue-cli-plugin-s3-deploy` handles creating `vue.config.js`. Re-run
`vue invoke s3-deploy` if you need to regenerate that file.
```
yarn deploy
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
