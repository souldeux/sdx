import Vue from 'vue'
import Vuetify from 'vuetify'
import Vuex from 'vuex'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Axios from 'axios';

Vue.config.productionTip = false
Vue.use(Vuetify, {
  theme: {
    primary: '#42b983'
  }
})
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    sdx_token: localStorage.getItem('sdx_token') || '',
  },
  mutations: {
    set_token(state, val) {
      state.sdx_token = val;
      localStorage.setItem('sdx_token', val);
    },
    clear_token(state) {
      state.sdx_token = ''
      localStorage.setItem('sdx_token', '')
    }
  },
  getters: {
    isLoggedIn: state => {
      return state.sdx_token !== ''
    },
    token: state => {
      return state.sdx_token
    }
  }
})

// Inject auth headers when user is logged in
axios.interceptors.request.use(request => {
  if (store.getters.isLoggedIn) {
    request.headers['Authorization'] = 'Token ' + store.getters.token
  }
  return request
})

axios.interceptors.response.use(response => response, error => {
  console.log(error)
  if (error.response.status === 401) console.log('Unauthorized; TODO redirect to login')
  return Promise.reject(error);
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')