import Vue from 'vue'
import VueRouter from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import MarkdownScratchpad from '@/components/MarkdownScratchpad'
import GitLabFeed from '@/components/GitLabFeed'
import SightWords from '@/components/SightWords/SightWords'
import Login from '@/components/Login'
import BlogList from '@/components/Blog/BlogList'
import BlogEdit from '@/components/Blog/BlogEdit'
import BlogDetail from '@/components/Blog/BlogDetail'
import axios from "axios";

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/scratch',
      name: 'MarkdownScratchpad',
      component: MarkdownScratchpad
    },
    {
      path: '/gitlabfeed',
      name: 'GitLabFeed',
      component: GitLabFeed
    },
    {
      path: '/sightwords',
      name: 'SightWords',
      component: SightWords
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/blog',
      name: 'Blog',
      component: BlogList
    },
    {
      path: '/blog/new',
      name: 'BlogNew',
      component: BlogEdit,
      beforeEnter: (to, from, next) => {
        allowStaffOnly(to, from, next);
      }
    },
    {
      path: '/blog/:slug',
      name: 'BlogDetail',
      component: BlogDetail
    },
    {
      path: '/blog/:slug/edit',
      name: 'BlogEdit',
      component: BlogEdit,
      beforeEnter: (to, from, next) => {
        allowStaffOnly(to, from, next);
      }
    }
  ]
})

function allowStaffOnly(to, from, next) {
  axios
    .get(process.env.VUE_APP_API_ROOT + "api/v1/users/account/")
    .then(response => {
      if (!response.data.staff) {
        next({ name: "Login" });
      } else {
        next();
      }
    })
    .catch(error => {
      next(error);
    });
}