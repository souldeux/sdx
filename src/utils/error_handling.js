export default function developmentCatch(error) {
  if (error.response) {
    /// The request was made and the server responsed with a status code that falls outside of the 2xx range
    console.log("Error data", error.response.data);
    console.log("Error status", error.response.status);
    console.log("Error headers", error.response.headers);
  } else if (error.request) {
    // The request was made but no response was received
    console.log("Error request", error.request);
  } else {
    // some other awful thing happened
    console.log("Error", error.message);
  }
  console.log("Error config", error.config);
}